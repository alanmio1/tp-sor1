//Crear un programa que simule ser un intérprete de comandos shell. Su programa debe imprimir un prompt en la terminal y luego debe quedarse esperando a que el usuario ingrese un comando shell. Dicho comando debe ejecutarse y mostrarse en la misma terminal. Puede usar las siguientes instrucciones://

#include <stdio.h>
#include <stdlib.h>
#include <string.h> // For strtok() and strcmp()
#include <unistd.h> // For fork(), pid_t



//para leer un comando input
#define MAX_COMMAND_LENGTH 100
char cmd[MAX_COMMAND_LENGTH]; //declaro un array de chars (cmd) de longitud 100 (la maxima longitud del comando a ejecutar sera 100)

#define MAX_NUMBER_OF_PARAMS 10
char* params[MAX_NUMBER_OF_PARAMS + 1];


void parseCmd(char* cmd, char** params)
{
    for(int i = 0; i < MAX_NUMBER_OF_PARAMS; i++)
    {
        params[i] = strsep(&cmd, " ");
        if(params[i] == NULL) break;
    }
}


// Main

int main()
{
    while (1)
    {
        printf("Ingrese comando:\n	> Shell$ "); 		//input, lee un comando 
        
        fgets(cmd, sizeof(cmd), stdin);	//remueve el enter para poder leer bien el comando

        if(cmd[strlen(cmd)-1] == '\n'){ //divide el comando en un array de parámetros
            cmd[strlen(cmd)-1] = '\0';
        }
	parseCmd(cmd, params);		//ejecutar los parámetros
        
	execvp(params[0], params);
    }
    return 0;
}
