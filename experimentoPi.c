#include<stdio.h>	//parahacer printf
#include<stdlib.h>	//para usar exit y funciones de libreria
#include<pthread.h>	//para usar thread
#include<semaphore.h>	//para usar semaforos

//compilar con gcc experimentoPi.c -o ejecutable
//ejecutar con  ./ejecutable

sem_t S_1erCuarto, S_2doCuarto, S_3erCuarto, S_Terminar;
//definir semaforos

        int aparicionDeNumeros[10];     //Variable Global
        int promedio = 0;               //Variable Global
        int numberArray[10000];         //Variable Global
	int Suma = 0;
	int NroMasFrecuenteEnPi = 0;
	int primosSuma = 0;

void Sumar()
{
        int i;
	for (i = 0; i < 8; i++){
		Suma = Suma + numberArray[i];
  	}
}

void* EscanearParte1(void*p)
{
   int i;
   int aux = 0;
   for(i=0;i<2500;i++)
   {		
  	aux = aux+numberArray[i];
	if (numberArray[i] == 0){
		aparicionDeNumeros[0] = aparicionDeNumeros[0]+1;
	}
	else if (numberArray[i] == 1){
		aparicionDeNumeros[1] = aparicionDeNumeros[1]+1;
	}
	else if (numberArray[i] == 2){
		aparicionDeNumeros[2] = aparicionDeNumeros[2]+1;
	}
	else if (numberArray[i] == 3){
		aparicionDeNumeros[3] = aparicionDeNumeros[3]+1;
	}
	else if (numberArray[i] == 4){
		aparicionDeNumeros[4] = aparicionDeNumeros[4]+1;
	}
	else if (numberArray[i] == 5){
		aparicionDeNumeros[5] = aparicionDeNumeros[5]+1;
	}
	else if (numberArray[i] == 6){
		aparicionDeNumeros[6] = aparicionDeNumeros[6]+1;
	}
	else if (numberArray[i] == 7){
		aparicionDeNumeros[7] = aparicionDeNumeros[7]+1;
	}
	else if (numberArray[i] == 8){
		aparicionDeNumeros[8] = aparicionDeNumeros[8]+1;
	}
	else if (numberArray[i] == 9){
		aparicionDeNumeros[9] = aparicionDeNumeros[9]+1;
	}
   }
   promedio = promedio + aux;
   sem_post(&S_1erCuarto);
   
   pthread_exit(NULL);
}
void* EscanearParte2(void*p)
{
   int i;
   int aux = 0;
   for(i=2500;i<5000;i++)
   {		
  	aux = aux+numberArray[i];
	if (numberArray[i] == 0){
		aparicionDeNumeros[0] = aparicionDeNumeros[0]+1;
	}
	else if (numberArray[i] == 1){
		aparicionDeNumeros[1] = aparicionDeNumeros[1]+1;
	}
	else if (numberArray[i] == 2){
		aparicionDeNumeros[2] = aparicionDeNumeros[2]+1;
	}
	else if (numberArray[i] == 3){
		aparicionDeNumeros[3] = aparicionDeNumeros[3]+1;
	}
	else if (numberArray[i] == 4){
		aparicionDeNumeros[4] = aparicionDeNumeros[4]+1;
	}
	else if (numberArray[i] == 5){
		aparicionDeNumeros[5] = aparicionDeNumeros[5]+1;
	}
	else if (numberArray[i] == 6){
		aparicionDeNumeros[6] = aparicionDeNumeros[6]+1;
	}
	else if (numberArray[i] == 7){
		aparicionDeNumeros[7] = aparicionDeNumeros[7]+1;
	}
	else if (numberArray[i] == 8){
		aparicionDeNumeros[8] = aparicionDeNumeros[8]+1;
	}
	else if (numberArray[i] == 9){
		aparicionDeNumeros[9] = aparicionDeNumeros[9]+1;
	}
   }
   sem_wait(&S_1erCuarto);
   promedio = promedio + aux;
   sem_post(&S_2doCuarto);
   
   pthread_exit(NULL);
}
void* EscanearParte3(void*p)
{
   int i;
   int aux = 0;
   for(i=5000;i<7500;i++)
   {		
  	aux = aux+numberArray[i];
	if (numberArray[i] == 0){
		aparicionDeNumeros[0] = aparicionDeNumeros[0]+1;
	}
	else if (numberArray[i] == 1){
		aparicionDeNumeros[1] = aparicionDeNumeros[1]+1;
	}
	else if (numberArray[i] == 2){
		aparicionDeNumeros[2] = aparicionDeNumeros[2]+1;
	}
	else if (numberArray[i] == 3){
		aparicionDeNumeros[3] = aparicionDeNumeros[3]+1;
	}
	else if (numberArray[i] == 4){
		aparicionDeNumeros[4] = aparicionDeNumeros[4]+1;
	}
	else if (numberArray[i] == 5){
		aparicionDeNumeros[5] = aparicionDeNumeros[5]+1;
	}
	else if (numberArray[i] == 6){
		aparicionDeNumeros[6] = aparicionDeNumeros[6]+1;
	}
	else if (numberArray[i] == 7){
		aparicionDeNumeros[7] = aparicionDeNumeros[7]+1;
	}
	else if (numberArray[i] == 8){
		aparicionDeNumeros[8] = aparicionDeNumeros[8]+1;
	}
	else if (numberArray[i] == 9){
		aparicionDeNumeros[9] = aparicionDeNumeros[9]+1;
	}
   }
   sem_wait(&S_2doCuarto);
   promedio = promedio + aux;
   sem_post(&S_3erCuarto);
   
   pthread_exit(NULL);
}
void* EscanearParte4(void*p)
{
   int i;
   int aux = 0;
   for(i=7500;i<10000;i++)
   {		
  	aux = aux+numberArray[i];
	if (numberArray[i] == 0){
		aparicionDeNumeros[0] = aparicionDeNumeros[0]+1;
	}
	else if (numberArray[i] == 1){
		aparicionDeNumeros[1] = aparicionDeNumeros[1]+1;
	}
	else if (numberArray[i] == 2){
		aparicionDeNumeros[2] = aparicionDeNumeros[2]+1;
	}
	else if (numberArray[i] == 3){
		aparicionDeNumeros[3] = aparicionDeNumeros[3]+1;
	}
	else if (numberArray[i] == 4){
		aparicionDeNumeros[4] = aparicionDeNumeros[4]+1;
	}
	else if (numberArray[i] == 5){
		aparicionDeNumeros[5] = aparicionDeNumeros[5]+1;
	}
	else if (numberArray[i] == 6){
		aparicionDeNumeros[6] = aparicionDeNumeros[6]+1;
	}
	else if (numberArray[i] == 7){
		aparicionDeNumeros[7] = aparicionDeNumeros[7]+1;
	}
	else if (numberArray[i] == 8){
		aparicionDeNumeros[8] = aparicionDeNumeros[8]+1;
	}
	else if (numberArray[i] == 9){
		aparicionDeNumeros[9] = aparicionDeNumeros[9]+1;
	}
   }
   sem_wait(&S_3erCuarto);
   promedio = promedio + aux;
   sem_post(&S_Terminar);
   
   pthread_exit(NULL);
}

void masFrecuente()
{
        int i;
	for (i = 1; i < 10;i++)
    	{
		if (aparicionDeNumeros[NroMasFrecuenteEnPi] < 					aparicionDeNumeros[i])
		{
		        NroMasFrecuenteEnPi = i;
		}
	}


}

void sumaPrimos()
{
	primosSuma = aparicionDeNumeros[2]+aparicionDeNumeros[3]+
	aparicionDeNumeros[5]+aparicionDeNumeros[7];
}

void imprimir()
{
	
	printf("La suma de los primeros digitos es: %d\n\n",Suma);
	
  	printf("El promedio de los primeros 10.000 numeros de Pi es: 			%d\n\n",promedio);

	printf("El numero 0 se encontro: %d\n\n",aparicionDeNumeros[0]);
	printf("El numero 1 se encontro: %d\n\n",aparicionDeNumeros[1]);
	printf("El numero 2 se encontro: %d\n\n",aparicionDeNumeros[2]);
	printf("El numero 3 se encontro: %d\n\n",aparicionDeNumeros[3]);
	printf("El numero 4 se encontro: %d\n\n",aparicionDeNumeros[4]);
	printf("El numero 5 se encontro: %d\n\n",aparicionDeNumeros[5]);
	printf("El numero 6 se encontro: %d\n\n",aparicionDeNumeros[6]);
	printf("El numero 7 se encontro: %d\n\n",aparicionDeNumeros[7]);
	printf("El numero 8 se encontro: %d\n\n",aparicionDeNumeros[8]);
	printf("El numero 9 se encontro: %d\n\n",aparicionDeNumeros[9]);

    	printf("El numero que mas se repite en Pi es el %d\n\n", 				NroMasFrecuenteEnPi);
	
    	printf("La suma de los numeros primos es %d\n\n",primosSuma);
}

void* funciones(void*p)
{
        sem_wait(&S_Terminar);
        promedio = promedio/10000;
	masFrecuente();
	sumaPrimos();
	imprimir();
}


int main(){
  
        sem_init(&S_1erCuarto,0,0);
        sem_init(&S_2doCuarto,0,0);
        sem_init(&S_3erCuarto,0,0);
        sem_init(&S_Terminar,0,0);

  FILE *myFile;
  myFile = fopen("10milDigitosDePi_separados.txt", "r");

  //leer archivo en un array
  int i;

  if (myFile == NULL){
    printf("Error en la lectura\n");
    exit (0);
  }

  for (i = 0; i < 10000; i++){
    fscanf(myFile, "%d,", &numberArray[i] );
  }

  printf("Los primeros digitos son\n\n");
  for (i = 0; i < 8; i++){
    printf("Numero es: %d\n\n", numberArray[i]);
  }
  printf("El ultimo numero es: %d\n\n", numberArray[9999]);

/////////////////////////////////////////////////////////////////
	Sumar(&Suma);
	
        pthread_t t1,t2,t3,t4,t5;//defnir threads

        pthread_create(&t1,NULL,EscanearParte1,NULL);
        pthread_create(&t2,NULL,EscanearParte2,NULL);
        pthread_create(&t3,NULL,EscanearParte3,NULL);
        pthread_create(&t4,NULL,EscanearParte4,NULL);
        pthread_create(&t5,NULL,funciones,NULL);
  
        pthread_join(t1,NULL);
        pthread_join(t2,NULL);
        pthread_join(t3,NULL);
        pthread_join(t4,NULL);
        pthread_join(t5,NULL);

  fclose(myFile);

  return 0;
}
