#include<stdio.h>	//parahacer printf
#include<stdlib.h>	//para usar exit y funciones de libreria
#include<pthread.h>	//para usar thread
#include<semaphore.h>	//para usar semaforos


#define ITERACIONES 2

sem_t S_descansar,S_jugar,S_pog,S_ultimoDescansar;//definir semaforos

void* Jugar(void*p)
{
	int i;
	for(i=0;i<ITERACIONES*2;i++) //Y EL DE DESCANSAR
	{
	sem_wait(&S_descansar);
	printf("\njugar\n");
	sem_post(&S_jugar);
	}
	pthread_exit(NULL);
}

void* Ganar(void*p)

{
	int i;
	for(i=0;i<ITERACIONES;i++) //Y EL DE DESCANSAR
	{
	sem_wait(&S_jugar);
	printf("\nganar\n");
	sem_post(&S_pog);
	}
	pthread_exit(NULL);
}

void* Perder(void*p)

{
	int i;
	for(i=0;i<ITERACIONES;i++) //Y EL DE DESCANSAR
	{
	sem_wait(&S_jugar);
	printf("\nperder\n");
	sem_post(&S_pog);
	}
	pthread_exit(NULL);
}

void* Descansar(void*p)

{
	int i;
	for(i=0;i<ITERACIONES*2;i++)
	{
	sem_wait(&S_pog);
	printf("\ndescansar\n");
	sem_post(&S_descansar);
	if (i == (ITERACIONES*2)-1){
		sem_post(&S_ultimoDescansar);
		}
	}
	pthread_exit(NULL);
}

void* Terminar(void*p)
{
	sem_wait(&S_ultimoDescansar);
	printf("\nterminar\n\n");
}

int main()
{
sem_init(&S_descansar,0,1);
sem_init(&S_jugar,0,0);
sem_init(&S_pog,0,0);
sem_init(&S_ultimoDescansar,0,0);

pthread_attr_t attr; struct sched_param param;
pthread_attr_init(&attr);/*Setear lo atributos por defecto*/
pthread_attr_getschedparam(&attr,&param);/*Obtener los parametros de scheduling*/
(param.sched_priority)++;/*aumentar la prioridad*/
pthread_attr_setschedparam(&attr,&param);/*setear el nuevo parametro*/

pthread_t t1,t2,t3,t4,t5;//defnir threads

pthread_create(&t1,NULL,Jugar,NULL);
pthread_create(&t2,&attr,Ganar,NULL);/*se le asigna prioridad*/
pthread_create(&t3,NULL,Perder,NULL);
pthread_create(&t4,NULL,Descansar,NULL);
pthread_create(&t5,NULL,Terminar,NULL);


pthread_join(t1,NULL);
pthread_join(t2,NULL);
pthread_join(t3,NULL);
pthread_join(t4,NULL);
pthread_join(t5,NULL);

return 0;
}
